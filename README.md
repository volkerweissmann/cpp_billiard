# Preparing the build directory

`meson setup build` or
`meson setup build -DUSE_MATPLOTLIB=false` or
`meson setup build -DUSE_MATPLOTLIB=true`

# Formatting the sources

`cd` into `build`, then run `ninja clang-format`

# Checking the formatting of the sources

`cd` into `build`, then run `ninja clang-format-check`

# Running the clan static analyser

`cd` into `build`, then run `ninja scan-build`

# Building everything

`cd` into `build`, then run `ninja`

# Running it

`cd` into `build`, then run `./cpp_billiard`
