#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <optional>
#include <tuple>
#include <vector>

#if USE_MATPLOTLIB
#include "matplotlib-cpp/matplotlibcpp.h"
namespace plt = matplotlibcpp;
#endif

template <typename T>
int signum(T val)
{
    return (T(0) < val) - (val < T(0));
}

class Vector {
public:
    double x;
    double y;
    Vector(const double x, const double y)
        : x(x)
        , y(y)
    {
    }
    double abs() const
    {
        return sqrt(x * x + y * y);
    }
    double cross(const Vector rhs) const
    {
        return x * rhs.y - y * rhs.x;
    }
    Vector operator/(const double rhs) const
    {
        return Vector(x / rhs, y / rhs);
    }
    Vector operator*(const double rhs) const
    {
        return Vector(x * rhs, y * rhs);
    }
    Vector rotated(const double angle) const
    {
        return Vector(x * cos(angle) - y * sin(angle), x * sin(angle) + y * cos(angle));
    }
    /// Calculates the dot product a.k.a. the scalar product between this and another Vector
    double dot(const Vector vec) const
    {
        return x * vec.x + y * vec.y;
    }
};

Vector operator*(double const scalar, const Vector vec)
{
    return Vector(scalar * vec.x, scalar * vec.y);
}

class Point {

public:
    double x;
    double y;

    Point(const double x, const double y)
        : x(x)
        , y(y)
    {
    }
    Vector operator-(const Point rhs) const
    {
        return Vector(x - rhs.x, y - rhs.y);
    }
    Point operator+(const Vector rhs) const
    {
        return Point(x + rhs.x, y + rhs.y);
    }
    Point operator-(const Vector rhs) const
    {
        return Point(x - rhs.x, y - rhs.y);
    }
};
/// A Circle is the set of all points p that fulfill pow((p-center).abs(), 2) = radius
class Circle {
public:
    Point center;
    double radius;

    Circle(const Point center, const double radius)
        : center(center)
        , radius(radius)
    {
    }
    std::tuple<std::vector<double>, std::vector<double>> discretize(const unsigned resolution) const
    {
        std::vector<double> x;
        std::vector<double> y;
        for (unsigned i = 0; i < resolution; i += 1) {
            x.push_back(center.x + radius * sin((double)i / (resolution - 1) * 2 * M_PI));
            y.push_back(center.y + radius * cos((double)i / (resolution - 1) * 2 * M_PI));
        }
        return { x, y };
    }
};
struct LineCircleDistance {
    double d;
};
/// A Section is the set of all points between the points p1 and p2
class Section {
public:
    Point p1;
    Point p2;
    Section(const Point p1, const Point p2)
        : p1(p1)
        , p2(p2)
    {
    }
    std::tuple<std::vector<double>, std::vector<double>> discretize(const unsigned resolution) const
    {
        std::vector<double> x;
        std::vector<double> y;
        for (unsigned i = 0; i < resolution; i += 1) {
            x.push_back(p1.x + (p2.x - p1.x) * i / (double)(resolution - 1));
            y.push_back(p1.y + (p2.y - p1.y) * i / (double)(resolution - 1));
        }
        return { x, y };
    }
};
/// A Ray is the list of all points p that fulfill p = position + direction * o
/// where o is a non-negative real number, ordered by increasing o
class Ray {
public:
    Point position;
    Vector direction; // not normed
    /// Computes the minimum signed distance between this Ray and a Circle, if the line
    /// could pass through all obstacles
    /// The returned value is positive if the circle center is on the right hand
    /// side of the ray
    /// If the ray moves away from the circle, std::nullopt is returned
    std::optional<LineCircleDistance> min_distance(const Circle obstacle) const
    {
        if (direction.dot(obstacle.center - position) < 0) {
            return std::nullopt;
        }
        return LineCircleDistance { (direction / direction.abs()).cross(position - obstacle.center) };
    }
    Ray(const Point position, const Vector direction)
        : position(position)
        , direction(direction)
    {
    }
    Section bounce(const Circle obstacle, const LineCircleDistance d)
    {
        const double angle = asin(d.d / obstacle.radius);
        const auto intersection = obstacle.center - obstacle.radius * (direction / direction.abs()).rotated(-angle);
        const auto ret = Section(position, intersection);
        position = intersection;
        direction = direction.rotated(M_PI - 2 * angle);
        return ret;
    }
    std::optional<Section> time_evolve(std::vector<Circle> obstacles)
    {
        // This code will break if a Ray intersects multiple obstacles
        for (const auto& obstacle : obstacles) {
            const auto dist = min_distance(obstacle);
            if (dist.has_value() && abs(dist.value().d) < obstacle.radius) {
                return bounce(obstacle, dist.value());
            }
        }
        return std::nullopt;
    }
};
int get_miss_sign(const std::vector<Circle> obstacles, const double b)
{
    Ray ray(Point(-10, b), Vector(1, 0));
    //time_evolve_and_visualize(ray, obstacles);

    int count = 0;
    int last_sign = 0;
    while (true) {
        // std::cout << count << " "
        //           << ray.min_distance(obstacles[0]).value_or(LineCircleDistance { 999 }).d << " "
        //           << ray.min_distance(obstacles[1]).value_or(LineCircleDistance { 999 }).d << " "
        //           << ray.min_distance(obstacles[2]).value_or(LineCircleDistance { 999 }).d << "\n";
        const auto obstacle = obstacles.at(count % 3);
        count += 1;
        const auto dist = ray.min_distance(obstacle);
        if (!dist.has_value()) {
            if (count % 2 == 0) {
                return -last_sign;
            } else {
                return last_sign;
            }
        } else if (abs(dist.value().d) < obstacle.radius) {
            ray.bounce(obstacle, dist.value());
            last_sign = signum(dist.value().d);
        } else {
            if (count % 2 == 0) {
                return signum(dist.value().d);
            } else {
                return -signum(dist.value().d);
            }
        }
    }
}
void time_evolve_and_visualize(const std::vector<Circle> obstacles, Ray ray, std::optional<std::ofstream> dout)
{
    {
        if (dout.has_value()) {
            dout.value() << ray.position.x << "\t" << ray.position.y << "\n";
        }
    }
#if USE_MATPLOTLIB
    for (unsigned i = 0; i < obstacles.size(); i++) {
        const auto [x, y] = obstacles.at(i).discretize(100);
        plt::plot(x, y);
    }
#endif
    int count = 0;
    while (true) {
        const auto sec = ray.time_evolve(obstacles);
        if (dout.has_value()) {
            dout.value() << ray.position.x << "\t" << ray.position.y << "\n";
        }
        if (sec.has_value()) {
#if USE_MATPLOTLIB
            const auto [x, y] = sec.value().discretize(2);
            plt::plot(x, y);
#endif
            if (dout.has_value()) {
            }
            count += 1;
        } else {
            break;
        }
    }
    {
        const auto last = Section(ray.position, ray.position + ray.direction * 10.);
#if USE_MATPLOTLIB
        const auto [x, y]
            = last.discretize(2);
        plt::plot(x, y);
#endif
        if (dout.has_value()) {
            dout.value() << last.p2.x << "\t" << last.p2.y << "\n";
        }
    }
#if USE_MATPLOTLIB
    std::cout << "plotting " << count << " sections\n";
    plt::show();
#endif
}
void generate_from_b(std::vector<Circle> obstacles, const double b, std::optional<std::ofstream> dout)
{
    std::cout << b << "\n";
    if (dout.has_value()) {
        dout.value().precision(std::numeric_limits<double>::max_digits10);
        dout.value() << "# b = " << b << "\n";
    }
    time_evolve_and_visualize(obstacles, Ray(Point(-10, b), Vector(1, 0)), std::move(dout));
}
std::tuple<double, double> bisect(const std::vector<Circle> obstacles, const double b_low, const double b_high)
{
    assert(b_high > b_low);
    assert(get_miss_sign(obstacles, b_low) == +1);
    assert(get_miss_sign(obstacles, b_high) == -1);
    const auto b_mid = (b_high + b_low) / 2;
    const auto sgn = get_miss_sign(obstacles, b_mid);
    if (b_mid == b_low || b_mid == b_high) {
        return { b_low, b_high };
    } else if (sgn == -1) {
        return bisect(obstacles, b_low, b_mid);
    } else if (sgn == +1) {
        return bisect(obstacles, b_mid, b_high);
    } else {
        assert(sgn == 0);
        return { b_low, b_high };
    }
}
int main()
{
    std::vector<Circle> obstacles;
    obstacles.push_back(Circle(Point(0, 0), 1));
    obstacles.push_back(Circle(
        Point(-6 * std::cos(30. / 180. * M_PI), +6 * std::sin(30. / 180. * M_PI)), 1));
    obstacles.push_back(Circle(
        Point(-6 * std::cos(30. / 180. * M_PI), -6 * std::sin(30. / 180. * M_PI)), 1));

    const auto [b_low, b_high] = bisect(obstacles, 0, 0.4);

    generate_from_b(obstacles, b_low, std::move(std::ofstream("b_low.dat")));
    generate_from_b(obstacles, b_high, std::move(std::ofstream("b_high.dat")));
}